<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="${pageContext.request.contextPath}/resources/images/Book.ico">

    <title>Bookshop</title>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/resources/css/offcanvas.css" rel="stylesheet">
</head>

<body>

<nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="${pageContext.request.contextPath}/user/page">Home <span class="sr-only">(current)</span></a>
            </li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a type="button" href="${pageContext.request.contextPath}/logout">Logout</a></li>
        </ul>

    </div>
</nav>

<div class="container">

    <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-12 col-md-9">
            <form action="${pageContext.request.contextPath}/user/orders/details/update" method="get">
                <table class="table table-striped">
                    <tr>
                        <th></th>
                        <th>OrderId</th>
                        <th>OrderInfoId</th>
                        <th>Book</th>
                        <th>Author</th>
                        <th>Quantity</th>


                    </tr>
                    <c:choose>
                        <c:when test="${empty orderInfo}">
                            <tr>
                                <td colspan="7">
                                    There are no details
                                </td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <c:forEach var="orderInfo" items="${orderInfo}">
                                <tr>
                                    <td><input type="radio" checked name="id"
                                               value="<c:out value="${orderInfo.orderInfoId}"/>">
                                    </td>
                                    <td><c:out value="${orderInfo.order.orderId}"/></td>
                                    <td><c:out value="${orderInfo.orderInfoId}"/></td>
                                    <td><c:out value="${orderInfo.book.title}"/></td>
                                    <td><c:out value="${orderInfo.book.author}"/></td>
                                    <td><c:out value="${orderInfo.quantity}"/></td>

                                </tr>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </table>

                <c:if test="${not empty orderInfo}">
                    <button type="submit" class="btn btn-default">Update</button>
                </c:if>
            </form>
        </div>
        <div class="col-6 col-md-3 sidebar-offcanvas" id="sidebar">
            <div class="list-group">

                <a href="${pageContext.request.contextPath}/user/news" class="list-group-item">News</a>
                <a href="${pageContext.request.contextPath}/user/basket" class="list-group-item">Cart</a>
                <a href="${pageContext.request.contextPath}/user/catalog" class="list-group-item">Catalog</a>
                <a href="${pageContext.request.contextPath}/user/profile" class="list-group-item">Profile</a>
                <a href="${pageContext.request.contextPath}/user/orders" class="list-group-item">Orders</a>

            </div>
        </div><!--/span-->
    </div><!--/row-->

    <hr>

    <footer>
        <p>&copy; Bookshop 2017</p>
    </footer>

</div><!--/.container-->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="${pageContext.request.contextPath}/resources/js/vendor/jquery.min.js"><\/script>')</script>
<script src="${pageContext.request.contextPath}/resources/js/vendor/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="${pageContext.request.contextPath}/resources/js/ie10-viewport-bug-workaround.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/offcanvas.js"></script>
</body>
</html>






