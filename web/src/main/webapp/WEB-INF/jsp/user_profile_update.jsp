<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="${pageContext.request.contextPath}/resources/images/Book.ico">

    <title>Bookshop</title>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/resources/css/offcanvas.css" rel="stylesheet">
</head>

<body>

<nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="${pageContext.request.contextPath}/user/page">Home <span class="sr-only">(current)</span></a>
            </li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a type="button" href="${pageContext.request.contextPath}/logout">Logout</a></li>
        </ul>

    </div>
</nav>

<div class="container">

    <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-12 col-md-9">
            <form:form method="post" modelAttribute="user" action="/user/profile">
                <div class="form-group">
                    <label for="userId"><h4>You can update personal information here</h4></label>
                    <p class="bg-danger"><form:errors path="userId"/></p>
                    <form:input id="userId" hidden="true" cssClass="form-control" path="userId" type="text"/>
                </div>
                <div class="form-group">
                    <label for="username"></label>
                    <p class="bg-danger"><form:errors path="username"/></p>
                    <form:input id="username" hidden="true" cssClass="form-control" path="username" type="text"/>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <p class="bg-danger"><form:errors path="password"/></p>
                    <form:input id="password" path="password" cssClass="form-control" type="password"/>
                </div>
                <div class="form-group">
                    <label for="confirmPassword">Confirm password</label>
                    <p class="bg-danger"><form:errors path="confirmPassword"/></p>
                    <form:input id="confirmPassword" path="confirmPassword" cssClass="form-control" type="password"/>
                </div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <p class="bg-danger"><form:errors path="name"/></p>
                    <form:input id="name" path="name" cssClass="form-control" type="text"/>
                </div>
                <div class="form-group">
                    <label for="surname">Surname</label>
                    <p class="bg-danger"><form:errors path="surname"/></p>
                    <form:input id="surname" path="surname" cssClass="form-control" type="text"/>
                </div>
                <div class="form-group">
                    <label for="fathername">Fathername</label>
                    <p class="bg-danger"><form:errors path="fathername"/></p>
                    <form:input id="fathername" path="fathername" cssClass="form-control" type="text"/>
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <p class="bg-danger"><form:errors path="address"/></p>
                    <form:input id="address" path="address" cssClass="form-control" type="text"/>
                </div>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <p class="bg-danger"><form:errors path="phone"/></p>
                    <form:input id="phone" path="phone" cssClass="form-control" type="text"/>
                </div>
                <div class="form-group">
                    <label for="extraInfo">Additional information</label>
                    <p class="bg-danger"><form:errors path="extraInfo"/></p>
                    <form:textarea path="extraInfo" id="extraInfo" cssClass="form-control" rows="5" cols="30"/>
                </div>
                <button class="btn btn-default">Update</button>
            </form:form>
        </div>
        <div class="col-6 col-md-3 sidebar-offcanvas" id="sidebar">
            <div class="list-group">

                <a href="${pageContext.request.contextPath}/user/news" class="list-group-item">News</a>
                <a href="${pageContext.request.contextPath}/user/basket" class="list-group-item">Cart</a>
                <a href="${pageContext.request.contextPath}/user/catalog" class="list-group-item">Catalog</a>
                <a href="${pageContext.request.contextPath}/user/profile" class="list-group-item">Profile</a>
                <a href="${pageContext.request.contextPath}/user/orders" class="list-group-item">Orders</a>

            </div>
        </div><!--/span-->
    </div><!--/row-->

    <hr>

    <footer>
        <p>&copy; Bookshop 2017</p>
    </footer>

</div><!--/.container-->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="${pageContext.request.contextPath}/resources/js/vendor/jquery.min.js"><\/script>')</script>
<script src="${pageContext.request.contextPath}/resources/js/vendor/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="${pageContext.request.contextPath}/resources/js/ie10-viewport-bug-workaround.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/offcanvas.js"></script>
</body>
</html>







