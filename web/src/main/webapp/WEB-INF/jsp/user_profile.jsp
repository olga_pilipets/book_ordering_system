<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="${pageContext.request.contextPath}/resources/images/Book.ico">

    <title>Bookshop</title>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/resources/css/offcanvas.css" rel="stylesheet">
</head>

<body>

<nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="${pageContext.request.contextPath}/user/page">Home <span class="sr-only">(current)</span></a>
            </li>


        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a type="button" href="${pageContext.request.contextPath}/logout">Logout</a></li>
        </ul>

    </div>
</nav>

<div class="container">

    <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-12 col-md-9">
            <form action="${pageContext.request.contextPath}/user/profile" method="post">
                <input type="hidden" name="id" id="id" value="${user.userId}">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" required class="form-control" id="username" name="username" disabled
                           value="<c:out value="${user.username}"/>">
                </div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" required class="form-control" id="name" name="name"
                           value="<c:out value="${user.name}"/>">
                </div>
                <div class="form-group">
                    <label for="fathername">Fathername</label>
                    <input type="text" required class="form-control" id="fathername" name="fathername"
                           value="<c:out value="${user.fathername}"/>">
                </div>
                <div class="form-group">
                    <label for="surname">Surname</label>
                    <input type="text" required class="form-control" id="surname" name="surname"
                           value="<c:out value="${user.surname}"/>">
                </div>
                <div class="form-group">
                    <label for="address">Adress</label>
                    <input type="text" required class="form-control" id="address" name="address"
                           value="<c:out value="${user.address}"/>">
                </div>
                <div class="form-group">
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="text" required class="form-control" id="phone" name="phone"
                               value="<c:out value="${user.phone}"/>">
                    </div>
                </div>
                <button type="submit" class="btn btn-default">Update</button>

            </form>
        </div>
        <div class="col-6 col-md-3 sidebar-offcanvas" id="sidebar">
            <div class="list-group">

                <a href="${pageContext.request.contextPath}/user/news" class="list-group-item">News</a>
                <a href="${pageContext.request.contextPath}/user/basket" class="list-group-item">Cart</a>
                <a href="${pageContext.request.contextPath}/user/catalog" class="list-group-item">Catalog</a>
                <a href="${pageContext.request.contextPath}/user/profile" class="list-group-item">Profile</a>
                <a href="${pageContext.request.contextPath}/user/orders" class="list-group-item">Orders</a>

            </div>
        </div><!--/span-->
    </div><!--/row-->

    <hr>

    <footer>
        <p>&copy; Bookshop 2017</p>
    </footer>

</div><!--/.container-->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="${pageContext.request.contextPath}/resources/js/vendor/jquery.min.js"><\/script>')</script>
<script src="${pageContext.request.contextPath}/resources/js/vendor/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="${pageContext.request.contextPath}/resources/js/ie10-viewport-bug-workaround.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/offcanvas.js"></script>
</body>
</html>







