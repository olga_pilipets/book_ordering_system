<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="${pageContext.request.contextPath}/resources/images/Book.ico">

    <title>Bookshop</title>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/resources/css/offcanvas.css" rel="stylesheet">
</head>

<body>

<nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="${pageContext.request.contextPath}/user">Home <span
                        class="sr-only">(current)</span></a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a type="button" href="${pageContext.request.contextPath}/logout">Logout</a></li>
        </ul>

    </div>
</nav>

<div class="container">

    <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-12 col-md-9">
            <p class="float-right d-md-none">
                <button type="button" class="btn btn-primary btn-sm" data-toggle="offcanvas">Toggle nav</button>
            </p>
            <div class="jumbotron">
                <h1>Wellcome to users page</h1>
                <p>We hope you'll enjoy buying our books</p>
            </div>
            <div class="row">
                <div class="col-5 col-lg-4">
                    <h2>News</h2>
                    <p>Read more about our latest activities </p>
                    <p><a class="btn btn-secondary" href="${pageContext.request.contextPath}/user/news" role="button">View
                        details &raquo;</a></p>
                </div><!--/span-->
                <div class="col-5 col-lg-4">
                    <h2>Cart</h2>
                    <p>All beloved books are saved here</p>
                    <p><a class="btn btn-secondary" href="${pageContext.request.contextPath}/user/basket" role="button">View
                        details&raquo;</a></p>
                </div><!--/span-->
                <div class="col-5 col-lg-4">
                    <h2>Catalog</h2>
                    <p>Want to read something new? The best books are here</p>
                    <p><a class="btn btn-secondary" href="${pageContext.request.contextPath}/user/catalog/{page}"
                          role="button">View details&raquo;</a></p>
                </div><!--/span-->
                <div class="col-5 col-lg-4">
                    <h2>Profile</h2>
                    <p>Users profile </p>
                    <p><a class="btn btn-secondary" href="${pageContext.request.contextPath}/user/profile"
                          role="button">View details&raquo;</a></p>
                </div><!--/span-->
                <div class="col-5 col-lg-4">
                    <h2>Orders</h2>
                    <p>You can find all your orders here </p>
                    <p><a class="btn btn-secondary" href="${pageContext.request.contextPath}/user/orders" role="button">View
                        details&raquo;</a></p>
                </div><!--/span-->
            </div><!--/row-->
        </div><!--/span-->

        <div class="col-6 col-md-3 sidebar-offcanvas" id="sidebar">
            <div class="list-group">

                <a href="${pageContext.request.contextPath}/user/news" class="list-group-item">News</a>
                <a href="${pageContext.request.contextPath}/user/basket" class="list-group-item">Cart</a>
                <a href="${pageContext.request.contextPath}/user/catalog/" class="list-group-item">Catalog</a>
                <a href="${pageContext.request.contextPath}/user/profile" class="list-group-item">Profile</a>
                <a href="${pageContext.request.contextPath}/user/orders" class="list-group-item">Orders</a>

            </div>
        </div><!--/span-->
    </div><!--/row-->

    <hr>

    <footer>
        <p>&copy; Bookshop 2017</p>
    </footer>

</div><!--/.container-->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="${pageContext.request.contextPath}/resources/js/vendor/jquery.min.js"><\/script>')</script>
<script src="${pageContext.request.contextPath}/resources/js/vendor/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="${pageContext.request.contextPath}/resources/js/ie10-viewport-bug-workaround.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/offcanvas.js"></script>
</body>
</html>




