<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="${pageContext.request.contextPath}/resources/images/Book.ico">

    <title>Bookshop</title>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/resources/css/offcanvas.css" rel="stylesheet">
</head>

<body>

<nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="${pageContext.request.contextPath}/user">Home <span
                        class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="${pageContext.request.contextPath}/feedback">Contact us</a>
            </li>

        </ul>

    </div>
</nav>


<div class="container-fluid">
    <c:if test="${not empty error}">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <p class="bg-info"><c:out value="${error}"/></p>
                <c:remove var="error" scope="session"/>
            </div>
            <div class="col-md-4"></div>
        </div>
    </c:if>
    <div class="row center-block">
        <div class="col-md-5"></div>
        <div class="col-md-2">
            <form:form method="post" modelAttribute="user" action="/register">
                <div class="form-group">
                    <label for="username">Email*</label>
                    <p class="bg-danger"><form:errors path="username"/></p>
                    <form:input id="username" cssClass="form-control" path="username" type="text"/>
                </div>
                <div class="form-group">
                    <label for="password">Password*</label>
                    <p class="bg-danger"><form:errors path="password"/></p>
                    <form:input id="password" path="password" cssClass="form-control" type="password"/>
                </div>
                <div class="form-group">
                    <label for="confirmPassword">Confirm password*</label>
                    <p class="bg-danger"><form:errors path="confirmPassword"/></p>
                    <form:input id="confirmPassword" path="confirmPassword" cssClass="form-control" type="password"/>
                </div>
                <div class="form-group">
                    <label for="name">Name*</label>
                    <p class="bg-danger"><form:errors path="name"/></p>
                    <form:input id="name" path="name" cssClass="form-control" type="text"/>
                </div>
                <div class="form-group">
                    <label for="surname">Surname*</label>
                    <p class="bg-danger"><form:errors path="surname"/></p>
                    <form:input id="surname" path="surname" cssClass="form-control" type="text"/>
                </div>
                <div class="form-group">
                    <label for="fathername">Fathername*</label>
                    <p class="bg-danger"><form:errors path="fathername"/></p>
                    <form:input id="fathername" path="fathername" cssClass="form-control" type="text"/>
                </div>
                <div class="form-group">
                    <label for="address">Address*</label>
                    <p class="bg-danger"><form:errors path="address"/></p>
                    <form:input id="address" path="address" cssClass="form-control" type="text"/>
                </div>
                <div class="form-group">
                    <label for="phone">Phone*</label>
                    <p class="bg-danger"><form:errors path="phone"/></p>
                    <form:input id="phone" path="phone" cssClass="form-control" type="text"/>
                </div>
                <div class="form-group">
                    <label for="extraInfo">Additional information</label>
                    <p class="bg-danger"><form:errors path="extraInfo"/></p>
                    <form:textarea path="extraInfo" id="extraInfo" cssClass="form-control" rows="5" cols="30"/>
                </div>
                <button class="btn btn-default">Add</button>
            </form:form>

        </div>
        <div class="col-md-5"></div>
    </div>
</div>


<hr>

<footer>
    <p>&copy; Bookshop 2017</p>
</footer>

</div><!--/.container-->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="${pageContext.request.contextPath}/resources/js/vendor/jquery.min.js"><\/script>')</script>
<script src="${pageContext.request.contextPath}/resources/js/vendor/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="${pageContext.request.contextPath}/resources/js/ie10-viewport-bug-workaround.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/offcanvas.js"></script>
</body>
</html>



