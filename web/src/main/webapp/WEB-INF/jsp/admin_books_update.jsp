<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="${pageContext.request.contextPath}/resources/images/Book.ico">

    <title>Bookshop</title>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/resources/css/offcanvas.css" rel="stylesheet">
</head>

<body>

<nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="${pageContext.request.contextPath}/admin/page">Home <span class="sr-only">(current)</span></a>
            </li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a type="button" href="${pageContext.request.contextPath}/logout">Logout</a></li>
        </ul>

    </div>
</nav>

<div class="container">

    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-12 col-md-9">
            <form action="${pageContext.request.contextPath}/admin/books/update" method="post">
                <input type="hidden" name="id" value="${book.bookId}">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" required class="form-control" id="title" name="title"
                           value="<c:out value="${book.title}"/>">
                </div>
                <div class="form-group">
                    <label for="author">Author</label>
                    <input type="text" required class="form-control" id="author" name="author"
                           value="<c:out value="${book.author}"/>">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <input type="text" required class="form-control" id="description" name="description"
                           value="<c:out value="${book.description}"/>">
                </div>
                <div class="form-group">
                    <label for="price">Price</label>
                    <input type="number" required class="form-control" id="price" name="price" placeholder="Price">
                </div>
                <button type="submit" class="btn btn-default">Update</button>
            </form>
        </div>


        <div class="col-6 col-md-3 sidebar-offcanvas" id="sidebar">
            <div class="list-group">

                <a href="${pageContext.request.contextPath}/admin/books" class="list-group-item">Books</a>
                <a href="${pageContext.request.contextPath}/admin/news" class="list-group-item">News</a>
                <a href="${pageContext.request.contextPath}/admin/orders" class="list-group-item">Orders</a>


            </div>
        </div><!--/span-->
    </div><!--/row-->

    <hr>

    <footer>
        <p>&copy; Bookshop 2017</p>
    </footer>

</div><!--/.container-->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="${pageContext.request.contextPath}/resources/js/vendor/jquery.min.js"><\/script>')</script>
<script src="${pageContext.request.contextPath}/resources/js/vendor/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="${pageContext.request.contextPath}/resources/js/ie10-viewport-bug-workaround.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/offcanvas.js"></script>
</body>
</html>








