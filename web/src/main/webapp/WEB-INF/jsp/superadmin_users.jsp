<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="${pageContext.request.contextPath}/resources/images/Book.ico">

    <title>Bookshop</title>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/resources/css/offcanvas.css" rel="stylesheet">
</head>

<body>

<nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="${pageContext.request.contextPath}/superadmin/page">Home <span
                        class="sr-only">(current)</span></a>
            </li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a type="button" href="${pageContext.request.contextPath}/logout">Logout</a></li>
        </ul>

    </div>
</nav>

<div class="container">

    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-12 col-md-9">
            <form action="${pageContext.request.contextPath}/superadmin/users/delete" method="post">
                <table class="table table-striped">
                    <tr>
                        <th></th>
                        <th>Id</th>
                        <th>Username</th>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Fathername</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th>Role</th>
                        <th>Status</th>
                        <th>Action</th>
                        <th>Action</th>
                    </tr>
                    <c:choose>
                        <c:when test="${empty users}">
                            <tr>
                                <td colspan="7">
                                    There are no users
                                </td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <c:forEach var="users" items="${users}">
                                <tr>
                                    <td><input type="radio" checked name="id" value="<c:out value="${users.userId}"/>">
                                    </td>
                                    <td><c:out value="${users.userId}"/></td>
                                    <td><c:out value="${users.username}"/></td>
                                    <td><c:out value="${users.name}"/></td>
                                    <td><c:out value="${users.surname}"/></td>
                                    <td><c:out value="${users.fathername}"/></td>
                                    <td><c:out value="${users.phone}"/></td>
                                    <td><c:out value="${users.address}"/></td>
                                    <td><c:out value="${users.role}"/></td>
                                    <td><c:out value="${users.status}"/></td>
                                    <td>
                                        <a href="${pageContext.request.contextPath}/superadmin/users/update?id=${users.userId}">Update</a>
                                    </td>
                                    <td>
                                        <a href="${pageContext.request.contextPath}/superadmin/users/block?id=${users.userId}">Block</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </table>
                <c:if test="${not empty users}">
                    <button type="submit" class="btn btn-default">Delete</button>
                </c:if>
            </form>
        </div>


    </div><!--/row-->

    <hr>

    <footer>
        <p>&copy; Bookshop 2017</p>
    </footer>

</div><!--/.container-->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="${pageContext.request.contextPath}/resources/js/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="${pageContext.request.contextPath}/resources/js/vendor/jquery.min.js"><\/script>')</script>
<script src="${pageContext.request.contextPath}/resources/js/vendor/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="${pageContext.request.contextPath}/resources/js/ie10-viewport-bug-workaround.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/offcanvas.js"></script>
</body>
</html>








