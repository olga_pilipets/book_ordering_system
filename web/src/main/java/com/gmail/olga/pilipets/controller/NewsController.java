package com.gmail.olga.pilipets.controller;

import com.gmail.olga.pilipets.NewsService;
import com.gmail.olga.pilipets.model.NewsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.List;

@Controller
public class NewsController {

    private final NewsService newsService;

    @Autowired
    public NewsController(NewsService newsService) {
        this.newsService = newsService;
    }

    @RequestMapping(value = "/user/news", method = RequestMethod.GET)
    public String showNewsForUser(Model model) {
        List<NewsDTO> news = newsService.findAll();
        model.addAttribute("news", news);
        return "user_news";
    }

    @RequestMapping(value = "/admin/news", method = RequestMethod.GET)
    public String showAllNewsAdmin(Model model) {
        List<NewsDTO> news = newsService.findAll();
        model.addAttribute("news", news);
        return "admin_news";
    }

    @RequestMapping(value = "/admin/news/delete", method = RequestMethod.POST)
    public String deleteNewsAdmin(Model model,
                             @RequestParam("id") Integer newsId) {
        NewsDTO newsDTO = newsService.findNewsByNewsId(newsId);
        newsService.deleteNews(newsDTO);
        return "redirect:/admin/news";
    }

    @RequestMapping(value = "/admin/news/add", method = RequestMethod.GET)
    public String addNewsAdmin(@ModelAttribute(value = "news") NewsDTO newsDTO) {
        return "admin_news_add";
    }

    @RequestMapping(value = "/admin/news/add", method = RequestMethod.POST)
    public String saveNewsAdmin(@ModelAttribute(value = "news") NewsDTO newsDTO) throws IOException {
        newsService.saveNews(newsDTO);
        return "redirect:/admin/news";
    }

    @RequestMapping(value = "/admin/news/update", method = RequestMethod.GET)
    public String updateNewsPageAdmin(Model model,
                                 @RequestParam(value = "id") String newsId) {

        NewsDTO news = newsService.findNewsByNewsId(Integer.valueOf(newsId));
        model.addAttribute("news", news);
        return "admin_news_update";

    }

    @RequestMapping(value = "/admin/news/update", method = RequestMethod.POST)
    public String updateNewsPageAdmin(Model model,
                                 @RequestParam(value = "id") String newsId,
                                 @RequestParam(value = "newsTitle") String newsTitle,
                                 @RequestParam(value = "newsText") String newsText
    ) {

        NewsDTO news = newsService.findNewsByNewsId(Integer.valueOf(newsId));
        news.setNewsTitle(newsTitle);
        news.setNewsText(newsText);
        newsService.updateNews(news);
        return "redirect:/admin/news";

    }


    @RequestMapping(value = "/superadmin/news", method = RequestMethod.GET)
    public String showAllNews(Model model) {
        List<NewsDTO> news = newsService.findAll();
        model.addAttribute("news", news);
        return "superadmin_news";
    }

    @RequestMapping(value = "/superadmin/news/delete", method = RequestMethod.POST)
    public String deleteNewsSuperAdmin(Model model,
                                  @RequestParam("id") Integer newsId) {
        NewsDTO newsDTO = newsService.findNewsByNewsId(newsId);
        newsService.deleteNews(newsDTO);
        return "redirect:/superadmin/news";
    }


    @RequestMapping(value = "/superadmin/news/add", method = RequestMethod.GET)
    public String addNews(@ModelAttribute(value = "news") NewsDTO newsDTO) {
        return "superadmin_news_add";
    }

    @RequestMapping(value = "/superadmin/news/add", method = RequestMethod.POST)
    public String saveNews(@ModelAttribute(value = "news") NewsDTO newsDTO) throws IOException {
        newsService.saveNews(newsDTO);
        return "redirect:/superadmin/news";
    }

    @RequestMapping(value = "/superadmin/news/update", method = RequestMethod.GET)
    public String updateNewsPage(Model model,
                                 @RequestParam(value = "id") String newsId) {

        NewsDTO news = newsService.findNewsByNewsId(Integer.valueOf(newsId));
        model.addAttribute("news", news);
        return "superadmin_news_update";

    }

    @RequestMapping(value = "/superadmin/news/update", method = RequestMethod.POST)
    public String updateNewsPage(Model model,
                                 @RequestParam(value = "id") String newsId,
                                 @RequestParam(value = "newsTitle") String newsTitle,
                                 @RequestParam(value = "newsText") String newsText
    ) {

        NewsDTO news = newsService.findNewsByNewsId(Integer.valueOf(newsId));
        news.setNewsTitle(newsTitle);
        news.setNewsText(newsText);
        newsService.updateNews(news);
        return "redirect:/superadmin/news";

    }

}
