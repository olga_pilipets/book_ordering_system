package com.gmail.olga.pilipets.util;


import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class EmailValidation {

    public static boolean isValidEmailAddress(String username) {
        boolean result = true;
        try {
            InternetAddress email = new InternetAddress(username);
            email.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }
}
