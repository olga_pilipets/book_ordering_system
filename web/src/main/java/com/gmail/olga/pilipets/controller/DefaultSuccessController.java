package com.gmail.olga.pilipets.controller;

import com.gmail.olga.pilipets.model.AppUserPrincipal;
import com.gmail.olga.pilipets.model.Role;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DefaultSuccessController {

    @RequestMapping(value = "/filter", method = RequestMethod.GET)
    public String filterDirectedUrl() {
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        if (principal.getRole() == Role.ROLE_USER) {
            return "redirect:/user/page";
        } else if (principal.getRole() == Role.ROLE_ADMIN) {
            return "redirect:/admin/page";
        } else if (principal.getRole() == Role.ROLE_SUPER_ADMIN) {
            return "redirect:/superadmin/page";
        } else {
            return "redirect:/login";
        }
    }
}
