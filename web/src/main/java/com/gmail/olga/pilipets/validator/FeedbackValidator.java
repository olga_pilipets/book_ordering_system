package com.gmail.olga.pilipets.validator;

import com.gmail.olga.pilipets.model.FeedbackDTO;
import com.gmail.olga.pilipets.util.EmailValidation;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class FeedbackValidator implements Validator{

        @Override
        public boolean supports(Class<?> aClass) {
        return aClass.equals(FeedbackDTO.class);
    }

        @Override
        public void validate(Object o, Errors errors) {
            FeedbackDTO feedback = (FeedbackDTO) o;
            if(!EmailValidation.isValidEmailAddress(feedback.getEmail())){
                errors.rejectValue("email", "error.not.valid.feedback.email");
            }
    }
}
