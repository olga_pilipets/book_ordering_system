package com.gmail.olga.pilipets.controller;

import com.gmail.olga.pilipets.UserService;
import com.gmail.olga.pilipets.model.UserDTO;
import com.gmail.olga.pilipets.validator.UserValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class RegisterController {

    private static final Logger logger = Logger.getLogger(RegisterController.class);

    private final UserService userService;
    private final UserValidator userValidator;

    @Autowired
    public RegisterController(
            UserService userService,
            UserValidator userValidator
    ) {
        this.userService = userService;
        this.userValidator = userValidator;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegisterPage(
            Model model) {
        logger.debug("Register page");
        model.addAttribute("user", new UserDTO());
        return "registration";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String saveUser(
            @ModelAttribute("user") UserDTO user,
            BindingResult result
    ) {
        userValidator.validate(user, result);
        if (!result.hasErrors()) {
            userService.saveUser(user);
            return "redirect:/login";
        } else {
            return "registration";
        }
    }
}
