package com.gmail.olga.pilipets.controller;

import com.gmail.olga.pilipets.BasketService;
import com.gmail.olga.pilipets.BookService;
import com.gmail.olga.pilipets.UserService;
import com.gmail.olga.pilipets.model.BookDTO;
import com.gmail.olga.pilipets.model.UserDTO;
import com.gmail.olga.pilipets.util.Principal;
import com.gmail.olga.pilipets.validator.BookValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;

@Controller
public class BooksController {

    private final BookService bookService;
    private final UserService userService;
    private final BasketService basketService;
    private final BookValidator bookValidator;

    @Autowired
    public BooksController(BookService bookService,
                           UserService userService,
                           BasketService basketService,
                           BookValidator bookValidator) {
        this.bookService = bookService;
        this.userService = userService;
        this.basketService = basketService;
        this.bookValidator = bookValidator;
    }

    @RequestMapping(value = "admin/books", method = RequestMethod.GET)
    public String showBooksAdmin(Model model, @ModelAttribute("book") BookDTO bookDTO,
                                 BindingResult result) {
        if (!result.hasErrors()) {
            List<BookDTO> books = bookService.findAllBooks();
            model.addAttribute("books", books);
            return "admin_books";
        } else {
            return "redirect:/admin/page";
        }
    }

    @RequestMapping(value = "admin/books/add", method = RequestMethod.GET)
    public String addBooksFormAdmin(@ModelAttribute("book") BookDTO bookDTO) {
        return "admin_books_add";
    }

    @RequestMapping(value = "admin/books/delete", method = RequestMethod.POST)
    public String deleteBooksAdmin(Model model,
                                   @RequestParam("id") Integer bookId) {
        BookDTO bookDTO = bookService.findById(bookId);
        bookService.deleteBook(bookDTO);
        return "redirect:/admin/books";
    }

    @RequestMapping(value = "admin/books/add", method = RequestMethod.POST)
    public String addBooksAdmin(Model model,
                                @ModelAttribute("book") BookDTO bookDTO,
                                BindingResult result) {
        bookValidator.validate(bookDTO,result);
        if (!result.hasErrors()) {
            bookService.addBook(bookDTO);
            return "redirect:/admin/books";
        } else {
            return "redirect:/admin/page";
        }
    }

    @RequestMapping(value = "admin/books/update", method = RequestMethod.GET)
    public String updateBookAdmin(Model model,
                                  @RequestParam(value = "id") Integer bookId) {

        BookDTO book = bookService.findById(bookId);
        model.addAttribute("book", book);
        return "admin_books_update";

    }

    @RequestMapping(value = "admin/books/update", method = RequestMethod.POST)
    public String updateBookAdmin(Model model,
                                  @RequestParam(value = "id") Integer bookId,
                                  @RequestParam(value = "title") String title,
                                  @RequestParam(value = "author") String author,
                                  @RequestParam(value = "description") String description,
                                  @RequestParam(value = "price") Double price) {

        BookDTO book = new BookDTO();
        book.setBookId(bookId);
        book.setTitle(title);
        book.setAuthor(author);
        book.setDescription(description);
        book.setPrice(price);
        bookService.updateBook(book);
        return "redirect:/admin/books";

    }

    @RequestMapping(value = "admin/books/copy", method = RequestMethod.GET)
    public String copyBookAdmin(Model model,
                                @RequestParam(value = "id") Integer id) {

        BookDTO book = bookService.findById(id);
        bookService.copyBook(book);
        return "redirect:/admin/books";

    }

    @RequestMapping(value = "/superadmin/books", method = RequestMethod.GET)
    public String showBooksSuperadmin(Model model, @ModelAttribute("book") BookDTO bookDTO,
                                      BindingResult result) {
        if (!result.hasErrors()) {
            List<BookDTO> books = bookService.findAllBooks();
            model.addAttribute("books", books);
            return "superadmin_books";
        } else {
            return "redirect:/superadmin/page";
        }
    }


    @RequestMapping(value = "/superadmin/books/add", method = RequestMethod.GET)
    public String addBooksFormSuperadmin(Model model,
                                         @ModelAttribute("book") BookDTO bookDTO,
                                         BindingResult result) {
        return "superadmin_books_add";
    }

    @RequestMapping(value = "/superadmin/books/delete", method = RequestMethod.POST)
    public String deleteBooksSuperadmin(Model model,
                                        @RequestParam("id") Integer bookId) {
        BookDTO bookDTO = bookService.findById(bookId);
        bookService.deleteBook(bookDTO);
        return "redirect:/superadmin/books";
    }

    @RequestMapping(value = "/superadmin/books/add", method = RequestMethod.POST)
    public String addBooksSuperadmin(Model model,
                                @ModelAttribute("book") BookDTO bookDTO,
                                BindingResult result) {
        bookValidator.validate(bookDTO,result);
        if (!result.hasErrors()) {
            bookService.addBook(bookDTO);
            return "redirect:/superadmin/books";
        } else {
            return "redirect:/superadmin/page";
        }
    }

    @RequestMapping(value = "/superadmin/books/update", method = RequestMethod.GET)
    public String updateBookSuperadmin(Model model,
                                       @RequestParam(value = "id") Integer bookId) {

        BookDTO book = bookService.findById(bookId);
        model.addAttribute("book", book);
        return "superadmin_books_update";

    }

    @RequestMapping(value = "/superadmin/books/update", method = RequestMethod.POST)
    public String updateBookSuperadmin(Model model,
                                       @RequestParam(value = "id") Integer bookId,
                                       @RequestParam(value = "title") String title,
                                       @RequestParam(value = "author") String author,
                                       @RequestParam(value = "description") String description,
                                       @RequestParam(value = "price") Double price) {
        BookDTO book = new BookDTO();
        book.setBookId(bookId);
        book.setTitle(title);
        book.setAuthor(author);
        book.setDescription(description);
        book.setPrice(price);
        bookService.updateBook(book);
        return "redirect:/superadmin/books";

    }

    @RequestMapping(value = "/superadmin/books/copy", method = RequestMethod.GET)
    public String copyBook(Model model,
                           @RequestParam(value = "id") Integer id) {

        BookDTO book = bookService.findById(id);
        bookService.copyBook(book);
        return "redirect:/superadmin/books";

    }

    @RequestMapping(value = "/user/catalog", method = RequestMethod.GET)
    public String showBookCatalog(Model model) {
        List<BookDTO> books = bookService.findAllBooks();
        model.addAttribute("books", books);
        return "user_catalog";
    }

    @RequestMapping(value = "/user/catalog/add", method = RequestMethod.GET)
    public String showBookCatalogAndAddToCart(Model model,
                                              @RequestParam(value = "id") Integer id) {
        BookDTO book = bookService.findById(id);
        UserDTO user = userService.getUserByUsername(Principal.getPrincipal().getUsername());
        basketService.saveBook(book, user);
        List<BookDTO> books = bookService.findAllBooks();
        model.addAttribute("books", books);
        return "user_catalog";
    }




}
