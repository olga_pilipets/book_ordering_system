package com.gmail.olga.pilipets.controller;


import com.gmail.olga.pilipets.*;
import com.gmail.olga.pilipets.model.*;
import com.gmail.olga.pilipets.util.Principal;
import com.gmail.olga.pilipets.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping(value = "/user")
public class UserController {
    private final UserService userService;
    private final UserValidator userValidator;

    @Autowired
    public UserController(UserService userService,
                          UserValidator userValidator) {
        this.userService = userService;
        this.userValidator=userValidator;
    }


    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public String showUserPage(Model model) {
        return "user";
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String showUserProfile(Model model) {
        UserDTO user = userService.getUserByUsername(Principal.getPrincipal().getUsername());
        System.out.println(user.getUsername());
        model.addAttribute("user", user);
        return "user_profile_update";
    }

    @RequestMapping(value = "/profile", method = RequestMethod.POST)
    public String updateUserProfile(@ModelAttribute(value = "user") UserDTO user, BindingResult result){
        System.out.println(user.getUsername());
     userValidator.validate(user,result);
        if (!result.hasErrors()) {
            userService.updateUser(user);
            return "redirect:/user/page";
        } else {
            return "user_profile_update";
        }
    }






}
