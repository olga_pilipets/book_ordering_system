package com.gmail.olga.pilipets.controller;

import com.gmail.olga.pilipets.OrderInfoService;
import com.gmail.olga.pilipets.OrderService;
import com.gmail.olga.pilipets.model.OrderDTO;
import com.gmail.olga.pilipets.model.OrderInfoDTO;
import com.gmail.olga.pilipets.model.Status;
import com.gmail.olga.pilipets.util.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class OrdersController {

    private final OrderService orderService;
    private final OrderInfoService orderInfoService;
@Autowired
    public OrdersController(OrderService orderService, OrderInfoService orderInfoService) {
        this.orderService = orderService;
        this.orderInfoService = orderInfoService;
    }

    @RequestMapping(value = "/admin/orders", method = RequestMethod.GET)
    public String showAllOrdersAdmin(Model model) {
        List<OrderDTO> orders = orderService.findAllOrders();
        model.addAttribute("orders", orders);
        return "admin_orders";
    }
    @RequestMapping(value = "/admin/orders/update", method = RequestMethod.GET)
    public String updateOrderPageAdmin(Model model,
                              @RequestParam(value = "id") Integer orderId) {

        OrderDTO order = orderService.findOrderByOrderId(orderId);
        model.addAttribute("order", order);
        model.addAttribute("statuses", Status.values());
        return "admin_orders_update";

    }

    @RequestMapping(value = "/admin/orders/update", method = RequestMethod.POST)
    public String updateOrderAdmin(Model model,
                                    @RequestParam(value = "id") Integer orderId,
                                    @RequestParam(value = "status") String status) {

        OrderDTO order = orderService.findOrderByOrderId(orderId);
        order.setStatus(Status.valueOf(status));
        orderService.updateOrderStatus(order);
        return "redirect:/admin/orders";

    }

    @RequestMapping(value = "/superadmin/orders", method = RequestMethod.GET)
    public String showAllOrders(Model model) {
        List<OrderDTO> orders = orderService.findAllOrders();
        model.addAttribute("orders", orders);
        return "superadmin_orders";
    }

    @RequestMapping(value = "/superadmin/orders/update", method = RequestMethod.GET)
    public String updateOrder(Model model,
                              @RequestParam(value = "id") Integer orderId) {

        OrderDTO order = orderService.findOrderByOrderId(orderId);
        model.addAttribute("order", order);
        model.addAttribute("statuses", Status.values());
        return "superadmin_orders_update";

    }

    @RequestMapping(value = "/superadmin/orders/update", method = RequestMethod.POST)
    public String updateOrderResult(Model model,
                                    @RequestParam(value = "id") Integer orderId,
                                    @RequestParam(value = "status") String status) {

        OrderDTO order = orderService.findOrderByOrderId(orderId);
        order.setStatus(Status.valueOf(status));
        orderService.updateOrderStatus(order);
        return "redirect:/superadmin/orders";

    }

    @RequestMapping(value = "/user/orders", method = RequestMethod.GET)
    public String showUserOrders(Model model) {
        List<OrderDTO> orders = orderService.findOrdersByUserId(Principal.getPrincipal().getUserId());
        model.addAttribute("orders", orders);
        return "user_orders";
    }

    @RequestMapping(value = "/user/orders/details", method = RequestMethod.GET)
    public String showUserOrderDetail(Model model,
                                      @RequestParam(value = "id") Integer id) {
        List<OrderInfoDTO> orderInfo = orderInfoService.findAllByOrderId(id);
        model.addAttribute("orderInfo", orderInfo);
        return "user_order_details";
    }

    @RequestMapping(value = "/user/orders/details/update", method = RequestMethod.GET)
    public String showUserOrderInfoForUpdating(Model model,
                                               @RequestParam(value = "id") Integer id) {
        OrderInfoDTO orderInfo = orderInfoService.findOrderInfoByOrderInfoId(id);
        model.addAttribute("orderInfo", orderInfo);
        return "user_orders_update";
    }

    @RequestMapping(value = "/user/orders/details/update", method = RequestMethod.POST)
    public String updateUserOrderInfo(@RequestParam(value = "orderInfoId")Integer orderInfoId,
                                      @RequestParam(value = "quantity") Integer quantity) {
        OrderInfoDTO orderInfoDTO=orderInfoService.findOrderInfoByOrderInfoId(orderInfoId);
        orderInfoDTO.setQuantity(quantity);
        orderInfoService.updateOrder(orderInfoDTO);
        return "redirect:/user/orders";
    }

}
