package com.gmail.olga.pilipets.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.gmail.olga.pilipets"})
public class AppConfig {
}
