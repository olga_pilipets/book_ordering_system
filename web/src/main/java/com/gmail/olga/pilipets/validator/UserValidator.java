package com.gmail.olga.pilipets.validator;

import com.gmail.olga.pilipets.model.UserDTO;
import com.gmail.olga.pilipets.util.EmailValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

@Component
public class UserValidator implements Validator {

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UserDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {

        UserDTO user = (UserDTO) o;

        ValidationUtils.rejectIfEmpty(errors, "username", "error.username.empty");
        ValidationUtils.rejectIfEmpty(errors, "password", "error.password.empty");
        ValidationUtils.rejectIfEmpty(errors, "confirmPassword", "error.confirm.password.empty");
        ValidationUtils.rejectIfEmpty(errors, "name", "error.name.empty");
        ValidationUtils.rejectIfEmpty(errors, "surname", "error.surname.empty");
        ValidationUtils.rejectIfEmpty(errors, "fathername", "error.fathername.empty");
        ValidationUtils.rejectIfEmpty(errors, "address", "error.address.empty");
        ValidationUtils.rejectIfEmpty(errors, "phone", "error.phone.empty");


        UserDetails userDetails = null;
        try {
            userDetails = userDetailsService.loadUserByUsername(user.getUsername());
        } catch (UsernameNotFoundException e) {

        }
        if ((userDetails != null)&&(user.getUserId()==null)) {
            errors.rejectValue("username", "error.username.exist");
        }
        if(!user.getPassword().equals(user.getConfirmPassword())){
            errors.rejectValue("password","error.not.equal.passwords");
        }
        if(!EmailValidation.isValidEmailAddress(user.getUsername())){
            errors.rejectValue("username", "error.not.valid.email");
        }
        if(!isValidPassword(user.getPassword())){
            errors.rejectValue("password", "error.not.valid.password");
        }
        if(!isValidPhoneNumber(user.getPhone())){
            errors.rejectValue("phone", "error.not.valid.phone");
        }
    }


    public boolean isValidPassword(String password) {
        String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
           boolean result=password.matches(pattern);
           return result;
        }

    public boolean isValidPhoneNumber(String phone) {
        String pattern = "\\(?\\+[0-9]{1,3}\\)? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{4}( ?-?[0-9]{3})? ?(\\w{1,10}\\s?\\d{1,6})?";
        boolean result=phone.matches(pattern);
        return result;
    }


}
