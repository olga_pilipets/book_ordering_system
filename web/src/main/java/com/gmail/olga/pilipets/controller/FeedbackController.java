package com.gmail.olga.pilipets.controller;

import com.gmail.olga.pilipets.FeedbackService;
import com.gmail.olga.pilipets.model.FeedbackDTO;
import com.gmail.olga.pilipets.validator.FeedbackValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller

public class FeedbackController {
    private final FeedbackService feedbackService;
    private final FeedbackValidator feedbackValidator;

    @Autowired
    public FeedbackController(FeedbackService feedbackService, FeedbackValidator feedbackValidator) {
        this.feedbackService = feedbackService;
        this.feedbackValidator = feedbackValidator;
    }

    @RequestMapping(value = "/feedback", method = RequestMethod.GET)
    public String showFeedBackPage(Model model) {
        model.addAttribute("feedback", new FeedbackDTO());
        return "feedback";
    }

    @RequestMapping(value = "/feedback", method = RequestMethod.POST)
    public String saveFeedBack(@ModelAttribute(value = "feedback") FeedbackDTO feedback,
                               BindingResult result) {
        feedbackValidator.validate(feedback, result);
        if (!result.hasErrors()) {
            System.out.println(feedback.getContent());
            feedbackService.save(feedback);
            return "redirect:/login";
        }
        return "feedback";

    }

    @RequestMapping(value = "superadmin/feedback", method = RequestMethod.GET)
    String showMessages(Model model) {
        List<FeedbackDTO> feedbackDTOList = feedbackService.getAllMessages();
        model.addAttribute("feedback", feedbackDTOList);
        return "superadmin_feedback";
    }
}
