package com.gmail.olga.pilipets.controller;

import com.gmail.olga.pilipets.*;
import com.gmail.olga.pilipets.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping(value = "/superadmin")
public class SuperAdminController {
    private final UserService userService;

    @Autowired
    public SuperAdminController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public String showAdminPage(Model model) {
        return "superadmin";
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String showUsers(Model model) {
        List<UserDTO> users = userService.getAll();
        model.addAttribute("users", users);
        return "superadmin_users";
    }

    @RequestMapping(value = "/users/delete", method = RequestMethod.POST)
    public String deleteUsers(Model model,
                              @RequestParam(value = "id") Integer userId) {
        UserDTO user = userService.getUserByUserId(userId);
        userService.deleteUser(user);
        return "redirect:/superadmin/users";
    }

    @RequestMapping(value = "/users/block", method = RequestMethod.GET)
    public String blockUsers(Model model,
                             @RequestParam(value = "id") Integer userId) {
        UserDTO user = userService.getUserByUserId(userId);
        userService.blockUser(user);
        return "redirect:/superadmin/users";
    }

    @RequestMapping(value = "/users/update", method = RequestMethod.GET)
    public String updateUsersPage(Model model,
                                  @RequestParam(value = "id") Integer userId) {
        UserDTO user = userService.getUserByUserId(userId);
        model.addAttribute("user", user);
        model.addAttribute("roles", Role.values());
        return "superadmin_users_update";
    }

    @RequestMapping(value = "/users/update", method = RequestMethod.POST)
    public String updateUser(Model model,
                             @RequestParam(value = "id") Integer userId,
                             @RequestParam(value = "password") String password,
                             @RequestParam(value = "role") String role) {
        UserDTO user = userService.getUserByUserId(userId);
        user.setRole(Role.valueOf(role));
        user.setPassword(password);
        userService.updateUserBySuperadmin(user);
        return "redirect:/superadmin/users";
    }


}
