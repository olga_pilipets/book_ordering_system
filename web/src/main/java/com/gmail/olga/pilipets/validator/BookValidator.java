package com.gmail.olga.pilipets.validator;

import com.gmail.olga.pilipets.model.BookDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


@Component
public class BookValidator implements Validator{
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(BookDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {

        BookDTO book = (BookDTO) o;
        ValidationUtils.rejectIfEmpty(errors,"title","book.title.empty");
        ValidationUtils.rejectIfEmpty(errors,"author","book.author.empty");
        ValidationUtils.rejectIfEmpty(errors,"price","book.price.empty");
    }
}

