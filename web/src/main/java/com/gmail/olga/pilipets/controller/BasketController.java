package com.gmail.olga.pilipets.controller;

import com.gmail.olga.pilipets.BasketService;
import com.gmail.olga.pilipets.OrderService;
import com.gmail.olga.pilipets.UserService;
import com.gmail.olga.pilipets.model.BasketDTO;
import com.gmail.olga.pilipets.model.UserDTO;
import com.gmail.olga.pilipets.util.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class BasketController {

    private final BasketService basketService;
    private final UserService userService;
    private final OrderService orderService;

    @Autowired
    public BasketController(BasketService basketService,
                            UserService userService,
                            OrderService orderService) {
        this.basketService = basketService;
        this.userService = userService;
        this.orderService = orderService;
    }

    @RequestMapping(value = "/user/basket", method = RequestMethod.GET)
    public String showUserBasket(Model model) {
        List<BasketDTO> basket = basketService.findBasketByUserId(Principal.getPrincipal().getUserId());
        Double totalCost = basketService.countTotalCost(basket);
        model.addAttribute("basket", basket);
        model.addAttribute("cost", totalCost);
        return "user_basket";
    }

    @RequestMapping(value = "/user/basket/delete", method = RequestMethod.POST)
    public String deleteUserBasket(Model model,
                                   @RequestParam(value = "id") Integer basketId) {
        BasketDTO basketDTO = basketService.findBasketByBasketId(basketId);
        basketService.deleteBasketById(basketDTO.getBasketId());
        return "redirect:/user/basket";
    }

    @RequestMapping(value = "/user/basket/save", method = RequestMethod.GET)
    public String saveUserOrder(Model model) {
        orderService.saveOrder();
        model.addAttribute("text", "The order has been created successefully");
        return "redirect:/user/orders";
    }

    @RequestMapping(value = "/user/basket/update", method = RequestMethod.GET)
    public String showUserBasket(Model model,
                                 @RequestParam(value = "id") Integer id) {
        BasketDTO basket = basketService.findBasketByBasketId(id);
        model.addAttribute("basket", basket);
        return "user_basket_update";
    }

    @RequestMapping(value = "/user/basket/update", method = RequestMethod.POST)
    public String showUserBasket(Model model,
                                 @RequestParam(value = "id") Integer id,
                                 @RequestParam(value = "quantity") Integer quantity) {
        UserDTO userDTO = userService.getUserByUsername(Principal.getPrincipal().getUsername());
        BasketDTO basketDTO = basketService.findBasketByBasketId(id);
        basketDTO.setQuantity(quantity);
        basketService.updateBasket(basketDTO, userDTO);
        return "redirect:/user/basket";

    }
}
