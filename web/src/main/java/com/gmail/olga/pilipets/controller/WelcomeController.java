package com.gmail.olga.pilipets.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class WelcomeController {

    @RequestMapping(value = {"/", "/login"}, method = RequestMethod.GET)
    public String showWelcomePage() {
        return "login";
    }

    @RequestMapping(value = {"/logout"}, method = RequestMethod.GET)
    public String showLoginPage() {
        return "login";
    }
}
