package com.gmail.olga.pilipets;

import java.io.File;

public interface PhotoService {

    File getFileById(Integer id);

}
