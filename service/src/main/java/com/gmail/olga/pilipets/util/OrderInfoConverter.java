package com.gmail.olga.pilipets.util;


import com.gmail.olga.pilipets.model.OrderInfoDTO;
import com.gmail.olga.pilipets.model.OrderInfo;

public class OrderInfoConverter {
    public OrderInfoConverter() {
    }

    public static OrderInfo converter(OrderInfoDTO orderInfoDTO) {
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setOrderInfoId(orderInfoDTO.getOrderInfoId());
        orderInfo.setBook(orderInfoDTO.getBook());
        orderInfo.setPrice(orderInfoDTO.getPrice());
        orderInfo.setQuantity(orderInfoDTO.getQuantity());
        orderInfo.setOrder(orderInfoDTO.getOrder());
        return orderInfo;
    }

    public static OrderInfo converter(OrderInfoDTO orderInfoDTO, OrderInfo orderInfo) {
        orderInfo.setQuantity(orderInfoDTO.getQuantity());
        return orderInfo;
    }

    public static OrderInfoDTO converter(OrderInfo orderInfo) {
        OrderInfoDTO orderInfoDTO = new OrderInfoDTO();
        orderInfoDTO.setOrderInfoId(orderInfo.getOrderInfoId());
        orderInfoDTO.setOrder(orderInfo.getOrder());
        orderInfoDTO.setQuantity(orderInfo.getQuantity());
        orderInfoDTO.setPrice(orderInfo.getQuantity());
        orderInfoDTO.setBook(orderInfo.getBook());
        return orderInfoDTO;
    }

}
