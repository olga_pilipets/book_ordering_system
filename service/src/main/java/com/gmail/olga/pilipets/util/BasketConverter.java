package com.gmail.olga.pilipets.util;


import com.gmail.olga.pilipets.model.BasketDTO;
import com.gmail.olga.pilipets.model.Basket;

public class BasketConverter {

    public static BasketDTO convert(Basket basket) {
        BasketDTO basketDTO = new BasketDTO();
        basketDTO.setBasketId(basket.getBasketId());
        basketDTO.setBook(basket.getBook());
        basketDTO.setQuantity(basket.getQuantity());
        basketDTO.setTotalCost(basket.getQuantity() * basket.getBook().getPrice());
        return basketDTO;
    }

    public static Basket convert(BasketDTO basketDTO){
        Basket basket=new Basket();
        basket.setBasketId(basketDTO.getBasketId());
        basket.setQuantity(basketDTO.getQuantity());
        basket.setBook(basketDTO.getBook());
        basket.setUser(basketDTO.getUser());
        return basket;
    }
}
