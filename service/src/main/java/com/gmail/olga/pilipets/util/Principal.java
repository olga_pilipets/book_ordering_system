package com.gmail.olga.pilipets.util;

import com.gmail.olga.pilipets.model.AppUserPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;

public class Principal {

    public static AppUserPrincipal getPrincipal() {
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        return principal;
    }
}
