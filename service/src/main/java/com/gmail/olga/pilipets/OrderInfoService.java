package com.gmail.olga.pilipets;


import com.gmail.olga.pilipets.model.OrderInfoDTO;

import java.util.List;

public interface OrderInfoService {
    List<OrderInfoDTO> findAllByOrderId(Integer id);
    OrderInfoDTO findOrderInfoByOrderInfoId(Integer id);
    void updateOrder(OrderInfoDTO orderInfoDTO);

}
