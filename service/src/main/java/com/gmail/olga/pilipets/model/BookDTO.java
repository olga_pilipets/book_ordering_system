package com.gmail.olga.pilipets.model;


public class BookDTO {
    private String title;
    private String author;
    private Integer bookId;
    private String description;
    private Double price;


    public BookDTO(){};

    public BookDTO(Book book){
        this.title=book.getTitle();
        this.author=book.getAuthor();
        this.bookId=book.getBookId();
        this.description=book.getDescription();
        this.price=book.getPrice();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }


}
