package com.gmail.olga.pilipets;


import com.gmail.olga.pilipets.model.FeedbackDTO;

import java.util.List;

public interface FeedbackService {
    void save(FeedbackDTO feedbackDTO);
    List<FeedbackDTO> getAllMessages();
}
