package com.gmail.olga.pilipets.util;

import com.gmail.olga.pilipets.model.NewsDTO;
import com.gmail.olga.pilipets.model.News;

public class NewsConverter {

    private NewsConverter() {
    }

    public static News convert(News news, NewsDTO newsDTO) {
        news.setDate(newsDTO.getDate());
        news.setNewsTitle(newsDTO.getNewsTitle());
        news.setNewsText(newsDTO.getNewsText());
        return news;
    }

    public static News convert(NewsDTO newsDTO) {
        News news = new News();
        news.setDate(newsDTO.getDate());
        news.setNewsTitle(newsDTO.getNewsTitle());
        news.setNewsText(newsDTO.getNewsText());
        return news;
    }

    public static NewsDTO convert(News news) {
        NewsDTO newsDTO = new NewsDTO();
        newsDTO.setDate(news.getDate());
        newsDTO.setNewsId(news.getNewsId());
        newsDTO.setNewsTitle(news.getNewsTitle());
        newsDTO.setNewsText(news.getNewsText());
        return newsDTO;
    }


}
