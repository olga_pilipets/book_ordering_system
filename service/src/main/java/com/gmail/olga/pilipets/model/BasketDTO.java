package com.gmail.olga.pilipets.model;


public class BasketDTO {
    private Integer basketId;
    private Integer quantity;
    private Double totalCost;
    private Book book;
    private User user;


    public Integer getBasketId() {
        return basketId;
    }

    public void setBasketId(Integer userId) {
        this.basketId = userId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
