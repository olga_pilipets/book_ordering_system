package com.gmail.olga.pilipets.impl;


import com.gmail.olga.pilipets.BasketService;
import com.gmail.olga.pilipets.util.BasketConverter;
import com.gmail.olga.pilipets.dao.BasketDao;
import com.gmail.olga.pilipets.dao.BookDao;
import com.gmail.olga.pilipets.dao.UserDao;
import com.gmail.olga.pilipets.model.BasketDTO;
import com.gmail.olga.pilipets.model.BookDTO;
import com.gmail.olga.pilipets.model.UserDTO;
import com.gmail.olga.pilipets.model.Basket;

import com.gmail.olga.pilipets.model.Book;
import com.gmail.olga.pilipets.model.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class BasketServiceImpl implements BasketService {

    private static final Logger logger = Logger.getLogger(BasketServiceImpl.class);

    private final BasketDao basketDao;
    private final BookDao bookDao;
    private final UserDao userDao;

    @Autowired
    public BasketServiceImpl(BasketDao basketDao, BookDao bookDao, UserDao userDao) {
        this.basketDao = basketDao;
        this.bookDao = bookDao;
        this.userDao = userDao;
    }

    @Override
    @Transactional
    public List<BasketDTO> findBasketByUserId(Integer userId) {
        List<Basket> baskets = basketDao.findBasketByUserId(userId);
        List<BasketDTO> basketsDTO = new ArrayList<>();
        for (Basket basket : baskets) {
            basketsDTO.add(BasketConverter.convert(basket));
        }
        return basketsDTO;
    }

    @Override
    @Transactional
    public void deleteBasketItem(BasketDTO basketDTO) {
        Basket basket = basketDao.findById(basketDTO.getBasketId());
        basketDao.delete(basket);
        logger.info("Basket item is deleted");
    }

    @Override
    @Transactional
    public void saveBook(BookDTO bookDTO, UserDTO userDTO) {
        Book book = bookDao.findById(bookDTO.getBookId());
        User user = userDao.findById(userDTO.getUserId());
        basketDao.addBooktoBasket(book, user);
        logger.info("Basket saved");

    }

    @Override
    @Transactional
    public void updateBasket(BasketDTO basketDTO, UserDTO userDTO) {
        User user = userDao.findById(userDTO.getUserId());
        Basket basket = BasketConverter.convert(basketDTO);
        basket.setUser(user);
        basketDao.saveOrUpdate(basket);
        logger.info("Basket updated");
    }

    @Override
    @Transactional
    public BasketDTO findBasketByBasketId(Integer id) {
        Basket basket = basketDao.findById(id);
        return BasketConverter.convert(basket);
    }

    @Override
    @Transactional
    public void deleteBasketById(Integer id) {
        Basket basket = basketDao.findById(id);
        basketDao.delete(basket);
        logger.info("Basket deleted");
    }

    @Override
    public Double countTotalCost(List<BasketDTO> basketDTOList) {
        Double totalBasketCost = 0.0;
        for (BasketDTO basketDTO : basketDTOList) {
            totalBasketCost = totalBasketCost + basketDTO.getQuantity() * basketDTO.getBook().getPrice();
        }
        return totalBasketCost;
    }
}
