package com.gmail.olga.pilipets.impl;

import com.gmail.olga.pilipets.UserService;
import com.gmail.olga.pilipets.util.Principal;
import com.gmail.olga.pilipets.util.UserConverter;
import com.gmail.olga.pilipets.dao.UserDao;
import com.gmail.olga.pilipets.model.UserDTO;
import com.gmail.olga.pilipets.model.UserStatus;
import com.gmail.olga.pilipets.model.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = Logger.getLogger(UserServiceImpl.class);

    private final UserDao userDao;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserDao userDao, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDao = userDao;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    @Transactional()
    public void saveUser(UserDTO userDTO) {
        User user = UserConverter.convert(userDTO);
        user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        userDao.save(user);
        logger.info("User saved");
    }

    @Override
    @Transactional
    public List<UserDTO> getAll() {
        List<User> users = userDao.findAll();
        List<UserDTO> userDTOList = new ArrayList<>();
        for (User user : users) {
            userDTOList.add(UserConverter.convert(user));
        }
        return userDTOList;
    }

    @Override
    @Transactional
    public void blockUser(UserDTO userDTO) {
        User user = userDao.findById(userDTO.getUserId());
        user.setUserStatus(UserStatus.BLOCKED);
        userDao.saveOrUpdate(user);
        logger.info("User blocked");

    }

    @Override
    @Transactional
    public void deleteUser(UserDTO userDTO) {
        User user = userDao.findById(userDTO.getUserId());
        userDao.delete(user);
        logger.info("User deleted");

    }

    @Override
    @Transactional
    public UserDTO getUserByUsername(String username) {
        User user = userDao.getUserByUsername(username);
        return UserConverter.convert(user);
    }

    @Override
    @Transactional
    public UserDTO getUserByUserId(Integer userId) {
        User user = userDao.findById(userId);
        return UserConverter.convert(user);
    }

    @Override
    @Transactional
    public void updateUser(UserDTO userDTO) {
        User user = UserConverter.convertForUpdate(userDTO);
        user.setUsername(Principal.getPrincipal().getUsername());
        user.setRole(Principal.getPrincipal().getRole());
        user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        userDao.saveOrUpdate(user);
        logger.info("User updated");
    }

    @Override
    @Transactional
    public void updateUserBySuperadmin(UserDTO userDTO) {
        User user = userDao.findById(userDTO.getUserId());
        user.setRole(userDTO.getRole());
        user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        userDao.saveOrUpdate(user);
        logger.info("User updated");
    }
}
