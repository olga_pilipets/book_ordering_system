package com.gmail.olga.pilipets.impl;

import com.gmail.olga.pilipets.OrderInfoService;
import com.gmail.olga.pilipets.util.OrderInfoConverter;
import com.gmail.olga.pilipets.dao.OrderDao;
import com.gmail.olga.pilipets.dao.OrderInfoDao;
import com.gmail.olga.pilipets.model.OrderInfoDTO;
import com.gmail.olga.pilipets.model.Status;
import com.gmail.olga.pilipets.model.Order;
import com.gmail.olga.pilipets.model.OrderInfo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderInfoServiceImpl implements OrderInfoService {

    private static final Logger logger = Logger.getLogger(OrderInfoServiceImpl.class);

    private final OrderInfoDao orderInfoDao;
    private final OrderDao orderDao;

    @Autowired
    public OrderInfoServiceImpl(OrderInfoDao orderInfoDao, OrderDao orderDao) {
        this.orderInfoDao = orderInfoDao;
        this.orderDao = orderDao;
    }

    @Override
    @Transactional
    public List<OrderInfoDTO> findAllByOrderId(Integer id) {

        List<OrderInfo> orderInfo = orderInfoDao.findAllByOrderId(id);
        List<OrderInfoDTO> orderInfoDTO = new ArrayList<>();
        for (OrderInfo info : orderInfo) {
            orderInfoDTO.add(OrderInfoConverter.converter(info));
        }
        return orderInfoDTO;
    }

    @Override
    @Transactional
    public OrderInfoDTO findOrderInfoByOrderInfoId(Integer id) {
        OrderInfo orderInfo = orderInfoDao.findById(id);
        return OrderInfoConverter.converter(orderInfo);
    }

    @Override
    @Transactional
    public void updateOrder(OrderInfoDTO orderInfoDTO) {
        if (orderInfoDTO.getOrder().getStatus().equals(Status.NEW)) {
            OrderInfo orderInfo = orderInfoDao.findById(orderInfoDTO.getOrderInfoId());
            OrderInfoConverter.converter(orderInfoDTO, orderInfo);
            orderInfoDao.save(orderInfo);
            List<OrderInfo> orderInfoList = orderInfoDao.findAllByOrderId(orderInfoDTO.getOrder().getOrderId());
            for (OrderInfo info : orderInfoList) {
                System.out.println(info.getQuantity());
                System.out.println(info.getPrice());
            }
            Order order = orderDao.findById(orderInfo.getOrder().getOrderId());
            Double totalOrderCost = 0.0;
            for (OrderInfo orderInfos : orderInfoList) {
                totalOrderCost = totalOrderCost + orderInfos.getQuantity() * orderInfos.getPrice();
            }
            order.setTotalCost(totalOrderCost);
            orderDao.saveOrUpdate(order);
            logger.info("Order updated");
        }
    }

}
