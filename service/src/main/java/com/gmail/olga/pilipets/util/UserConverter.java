package com.gmail.olga.pilipets.util;

import com.gmail.olga.pilipets.model.Role;
import com.gmail.olga.pilipets.model.UserDTO;
import com.gmail.olga.pilipets.model.UserStatus;
import com.gmail.olga.pilipets.model.User;

public class UserConverter {
    private UserConverter() {
    }

    public static User convert(UserDTO userDTO) {
        User user = new User();
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        user.setName(userDTO.getName());
        user.setSurname(userDTO.getSurname());
        user.setFathername(userDTO.getFathername());
        user.setAddress(userDTO.getAddress());
        user.setPhone(userDTO.getPhone());
        user.setRole(Role.ROLE_USER);
        user.setUserStatus(UserStatus.ACTIVE);
        user.setExtraInfo(userDTO.getExtraInfo());
        return user;
    }

    public static User convertForUpdate(UserDTO userDTO) {
        User user = new User();
        user.setId(userDTO.getUserId());
        user.setPassword(userDTO.getPassword());
        user.setName(userDTO.getName());
        user.setSurname(userDTO.getSurname());
        user.setFathername(userDTO.getFathername());
        user.setAddress(userDTO.getAddress());
        user.setPhone(userDTO.getPhone());
        user.setRole(userDTO.getRole());
        user.setExtraInfo(userDTO.getExtraInfo());
        return user;
    }


    public static UserDTO convert(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setUserId(user.getId());
        userDTO.setUsername(user.getUsername());
        userDTO.setPassword(user.getPassword());
        userDTO.setName(user.getName());
        userDTO.setSurname(user.getSurname());
        userDTO.setFathername(user.getFathername());
        userDTO.setAddress(user.getAddress());
        userDTO.setPhone(user.getPhone());
        userDTO.setRole(user.getRole());
        userDTO.setStatus(user.getUserStatus());
        userDTO.setExtraInfo(user.getExtraInfo());
        return userDTO;
    }
}
