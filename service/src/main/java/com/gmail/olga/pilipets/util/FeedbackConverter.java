package com.gmail.olga.pilipets.util;


import com.gmail.olga.pilipets.model.FeedbackDTO;
import com.gmail.olga.pilipets.model.Feedback;

public class FeedbackConverter {

    public static FeedbackDTO convert(Feedback feedback) {
        FeedbackDTO feedbackDTO = new FeedbackDTO();
        feedbackDTO.setId(feedback.getId());
        feedbackDTO.setEmail(feedback.getEmail());
        feedbackDTO.setContent(feedback.getContent());
        return feedbackDTO;
    }

    public static Feedback convert(FeedbackDTO feedbackDTO) {
        Feedback feedback = new Feedback();
        feedback.setEmail(feedbackDTO.getEmail());
        feedback.setContent(feedbackDTO.getContent());
        return feedback;
    }
}
