package com.gmail.olga.pilipets;

import com.gmail.olga.pilipets.model.UserDTO;

import java.util.List;

public interface UserService {

    void blockUser(UserDTO userDTO);

    void deleteUser(UserDTO userDTO);

    UserDTO getUserByUsername(String username);

    void saveUser(UserDTO userDTO);

    List<UserDTO> getAll();

    void updateUser(UserDTO userDTO);

    UserDTO getUserByUserId(Integer userId);

    void updateUserBySuperadmin(UserDTO userDTO);
}
