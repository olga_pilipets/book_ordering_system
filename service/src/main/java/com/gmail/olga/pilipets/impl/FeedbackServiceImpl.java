package com.gmail.olga.pilipets.impl;

import com.gmail.olga.pilipets.FeedbackService;
import com.gmail.olga.pilipets.dao.FeedbackDao;
import com.gmail.olga.pilipets.model.FeedbackDTO;
import com.gmail.olga.pilipets.model.Feedback;
import com.gmail.olga.pilipets.util.FeedbackConverter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class FeedbackServiceImpl implements FeedbackService {

    private static final Logger logger = Logger.getLogger(FeedbackServiceImpl.class);

    private final FeedbackDao feedbackDao;

    @Autowired
    public FeedbackServiceImpl(FeedbackDao feedbackDao) {
        this.feedbackDao = feedbackDao;
    }

    @Override
    @Transactional
    public void save(FeedbackDTO feedbackDTO) {
        Feedback feedback=FeedbackConverter.convert(feedbackDTO);
        System.out.println();
        feedbackDao.save(feedback);
        logger.info("Feedback saved");
    }

    @Override
    @Transactional
    public List<FeedbackDTO> getAllMessages() {
        List<Feedback> feedbackList = feedbackDao.findAll();
        List<FeedbackDTO> feedbackDTOList = new ArrayList<>();
        for (Feedback feedback : feedbackList) {
            feedbackDTOList.add(FeedbackConverter.convert(feedback));
        }
        return feedbackDTOList;
    }
}
