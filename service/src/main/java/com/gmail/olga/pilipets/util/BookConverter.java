package com.gmail.olga.pilipets.util;


import com.gmail.olga.pilipets.model.BookDTO;
import com.gmail.olga.pilipets.model.Book;

import java.util.ArrayList;
import java.util.List;

public class BookConverter {

    private BookConverter(){}

    public static Book convert(BookDTO bookDTO){
        Book book=new Book();
        book.setTitle(bookDTO.getTitle());
        book.setAuthor(bookDTO.getAuthor());
        book.setDescription(bookDTO.getDescription());
        book.setPrice(bookDTO.getPrice());
        return book;
    }

    public static Book convertforUpdate(BookDTO bookDTO){
        Book book=new Book();
        book.setBookId(bookDTO.getBookId());
        book.setTitle(bookDTO.getTitle());
        book.setAuthor(bookDTO.getAuthor());
        book.setDescription(bookDTO.getDescription());
        book.setPrice(bookDTO.getPrice());
        return book;
    }

    public static Book convert(Book book,BookDTO bookDTO){
        book.setTitle(bookDTO.getTitle());
        book.setAuthor(bookDTO.getAuthor());
        book.setDescription(bookDTO.getDescription());
        book.setPrice(bookDTO.getPrice());
        return book;
    }

    public static BookDTO convert(Book book){
        BookDTO bookDTO=new BookDTO();
        bookDTO.setBookId(book.getBookId());
        bookDTO.setPrice(book.getPrice());
        bookDTO.setTitle(book.getTitle());
        bookDTO.setAuthor(book.getAuthor());
        bookDTO.setDescription(book.getDescription());
        return bookDTO;
    }

    public static List<BookDTO> convert(List<Book> bookList){
        List<BookDTO> bookDTOList=new ArrayList<>();
        for (Book book : bookList) {
            bookDTOList.add(BookConverter.convert(book));
        }
        return bookDTOList;
    }
}

