package com.gmail.olga.pilipets;

import com.gmail.olga.pilipets.model.BasketDTO;
import com.gmail.olga.pilipets.model.OrderDTO;
import com.gmail.olga.pilipets.model.UserDTO;


import java.util.List;


public interface OrderService {
    List<OrderDTO> findAllOrders();

    List<OrderDTO> findOrdersByUserId(Integer id);

    OrderDTO findOrderByOrderId(Integer id);

    void updateOrderStatus(OrderDTO orderDTO);

    void saveOrder();

}
