package com.gmail.olga.pilipets;

import com.gmail.olga.pilipets.model.NewsDTO;

import java.io.IOException;
import java.util.List;


public interface NewsService {
    List<NewsDTO> findAll();
    void saveNews(NewsDTO newsDTO) throws IOException;
    NewsDTO findNewsByNewsId(Integer id);
    void updateNews(NewsDTO newsDTO);
    void deleteNews(NewsDTO newsDTO);

}
