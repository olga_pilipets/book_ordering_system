package com.gmail.olga.pilipets.util;

import com.gmail.olga.pilipets.model.OrderDTO;
import com.gmail.olga.pilipets.model.Order;

public class OrderConverter {
    public OrderConverter() {
    }

    public static Order converter(OrderDTO orderDTO){
        Order order=new Order();
        order.setTotalCost(orderDTO.getTotalCost());
        order.setStatus(orderDTO.getStatus());
        order.setUser(orderDTO.getUser());
        return order;
    }

    public static OrderDTO converter(Order order){
        OrderDTO orderDTO=new OrderDTO();
        orderDTO.setOrderId(order.getOrderId());
        orderDTO.setTotalCost(order.getTotalCost());
        orderDTO.setStatus(order.getStatus());
        orderDTO.setUser(order.getUser());
        return orderDTO;
    }

    public static Order convertForUpdate(OrderDTO orderDTO){
        Order order=new Order();
        order.setOrderId(orderDTO.getOrderId());
        order.setTotalCost(orderDTO.getTotalCost());
        order.setStatus(orderDTO.getStatus());
        order.setUser(orderDTO.getUser());
        return order;
    }
}
