package com.gmail.olga.pilipets.impl;


import com.gmail.olga.pilipets.BookService;
import com.gmail.olga.pilipets.util.BookConverter;
import com.gmail.olga.pilipets.dao.BookDao;
import com.gmail.olga.pilipets.model.BookDTO;
import com.gmail.olga.pilipets.model.Book;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    private static final Logger logger = Logger.getLogger(BookServiceImpl.class);

    private final BookDao bookDao;

    @Autowired
    public BookServiceImpl(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    @Override
    @Transactional
    public void updateBook(BookDTO bookDTO) {
        Book book = BookConverter.convertforUpdate(bookDTO);
        bookDao.saveOrUpdateBook(book);
        logger.info("Book updated");

    }

    @Override
    @Transactional
    public void copyBook(BookDTO bookDTO) {
        Book book = BookConverter.convert(bookDTO);
        book.setTitle(bookDTO.getTitle() + "copy");
        bookDao.save(book);
        logger.info("Book copied");
    }

    @Override
    @Transactional
    public BookDTO findById(Integer bookId) {
        Book book = bookDao.findById(bookId);
        BookDTO bookDTO = new BookDTO(book);
        return bookDTO;
    }

    @Override
    @Transactional
    public List<BookDTO> findAllBooks() {
        List<Book> books = bookDao.findAll();
        List<BookDTO> booksDTO = new ArrayList<>();
        for (Book book : books) {
            booksDTO.add(BookConverter.convert(book));
        }
        return booksDTO;
    }


    @Override
    @Transactional
    public void deleteBook(BookDTO bookDTO) {
        Book book = BookConverter.convertforUpdate(bookDTO);
        bookDao.delete(book);
    }

    @Override
    @Transactional
    public void addBook(BookDTO bookDTO) {
        Book book = BookConverter.convert(bookDTO);
        bookDao.save(book);

    }


}
