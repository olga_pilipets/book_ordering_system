package com.gmail.olga.pilipets;

import com.gmail.olga.pilipets.model.BasketDTO;
import com.gmail.olga.pilipets.model.BookDTO;
import com.gmail.olga.pilipets.model.UserDTO;

import java.util.List;


public interface BasketService {
    List<BasketDTO> findBasketByUserId(Integer userId);

    void deleteBasketItem(BasketDTO basketDTO);

    public void saveBook(BookDTO bookDTO, UserDTO userDTO);

    public void updateBasket(BasketDTO basketDTO, UserDTO userDTO);

    BasketDTO findBasketByBasketId(Integer id);
    void deleteBasketById(Integer id);
    Double countTotalCost(List<BasketDTO> basketDTOList);
}
