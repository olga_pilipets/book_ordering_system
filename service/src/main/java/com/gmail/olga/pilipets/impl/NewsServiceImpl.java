package com.gmail.olga.pilipets.impl;


import com.gmail.olga.pilipets.NewsService;
import com.gmail.olga.pilipets.util.NewsConverter;
import com.gmail.olga.pilipets.dao.NewsDao;
import com.gmail.olga.pilipets.model.NewsDTO;
import com.gmail.olga.pilipets.model.News;

import com.gmail.olga.pilipets.model.Photo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class NewsServiceImpl implements NewsService {

    private static final Logger logger = Logger.getLogger(NewsServiceImpl.class);

    @Autowired
    private NewsDao newsDao;

    @Autowired
    private Environment environment;


    @Override
    @Transactional
    public List<NewsDTO> findAll() {
        List<News> newsList = newsDao.findAll();
        List<NewsDTO> newsDTOList = new ArrayList<>();
        for (News news : newsList) {
            NewsDTO newsDTO = NewsConverter.convert(news);
            newsDTOList.add(newsDTO);
        }
        return newsDTOList;
    }


    @Override
    @Transactional
    public NewsDTO findNewsByNewsId(Integer id) {
        News news = newsDao.findById(id);
        return NewsConverter.convert(news);
    }


    @Override
    @Transactional
    public void updateNews(NewsDTO newsDTO) {
        News news = newsDao.findById(newsDTO.getNewsId());
        Date date = new Date();
        newsDTO.setDate(date);
        NewsConverter.convert(news, newsDTO);
        newsDao.saveOrUpdate(news);

    }

    @Override
    @Transactional
    public void deleteNews(NewsDTO newsDTO) {
        News news = newsDao.findById(newsDTO.getNewsId());
        newsDao.delete(news);
    }

    @Override
    @Transactional
    public void saveNews(NewsDTO newsDTO) throws IOException {
        Date date = new Date();
        newsDTO.setDate(date);
        News news = NewsConverter.convert(newsDTO);
        try {
            Photo photo = getPhoto(newsDTO);
            news.setPhoto(photo);
            newsDao.save(news);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
    }

    private Photo getPhoto(NewsDTO newsDTO) throws IOException {
        String photoLocation = environment.getProperty("upload.location") + System.currentTimeMillis();
        FileCopyUtils.copy(newsDTO.getPhoto().getBytes(), new File(photoLocation));
        Photo photo = new Photo();
        photo.setLocation(photoLocation);
        photo.setPhotoName(newsDTO.getPhoto().getName());
        return photo;
    }


}
