package com.gmail.olga.pilipets.impl;


import com.gmail.olga.pilipets.PhotoService;

import com.gmail.olga.pilipets.dao.PhotoDao;
import com.gmail.olga.pilipets.model.Photo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;

@Service
public class PhotoServiceImpl implements PhotoService {

    private static final Logger logger = Logger.getLogger(PhotoServiceImpl.class);

    private final PhotoDao photoDao;

    @Autowired
    public PhotoServiceImpl(PhotoDao photoDao) {
        this.photoDao = photoDao;
    }

    @Override
    @Transactional
    public File getFileById(Integer id) {
        Photo photo = photoDao.findById(id);
        return new File(photo.getLocation());
    }
}
