package com.gmail.olga.pilipets;

import com.gmail.olga.pilipets.model.BookDTO;

import java.util.List;


public interface BookService {

    void updateBook(BookDTO bookDTO);

    void addBook(BookDTO bookDTO);

    List<BookDTO> findAllBooks();

    BookDTO findById(Integer bookId);

    void copyBook(BookDTO bookDTO);

    void deleteBook(BookDTO bookDTO);
}
