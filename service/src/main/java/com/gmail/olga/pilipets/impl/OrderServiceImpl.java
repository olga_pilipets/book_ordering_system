package com.gmail.olga.pilipets.impl;


import com.gmail.olga.pilipets.OrderService;
import com.gmail.olga.pilipets.util.OrderConverter;
import com.gmail.olga.pilipets.util.Principal;
import com.gmail.olga.pilipets.dao.BasketDao;
import com.gmail.olga.pilipets.dao.OrderDao;
import com.gmail.olga.pilipets.dao.OrderInfoDao;
import com.gmail.olga.pilipets.dao.UserDao;
import com.gmail.olga.pilipets.model.*;
import com.gmail.olga.pilipets.model.Basket;
import com.gmail.olga.pilipets.model.OrderInfo;
import com.gmail.olga.pilipets.model.Order;
import com.gmail.olga.pilipets.model.User;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class OrderServiceImpl implements OrderService {

    private static final Logger logger = Logger.getLogger(OrderServiceImpl.class);

    private final OrderDao orderDao;
    private final UserDao userDao;
    private final BasketDao basketDao;
    private final OrderInfoDao orderInfoDao;

    @Autowired
    public OrderServiceImpl(OrderDao orderDao, UserDao userDao, BasketDao basketDao, OrderInfoDao orderInfoDao) {
        this.orderDao = orderDao;
        this.userDao = userDao;
        this.basketDao = basketDao;
        this.orderInfoDao = orderInfoDao;
    }


    @Override
    @Transactional
    public List<OrderDTO> findAllOrders() {
        List<Order> orders = orderDao.findAll();
        List<OrderDTO> ordersDTO = new ArrayList<>();
        for (Order order : orders) {
            ordersDTO.add(OrderConverter.converter(order));
        }
        return ordersDTO;
    }

    @Override
    @Transactional
    public List<OrderDTO> findOrdersByUserId(Integer id) {
        List<Order> orders = orderDao.findOrdersByUserId(id);
        List<OrderDTO> ordersDTO = new ArrayList<>();
        for (Order order : orders) {
            ordersDTO.add(OrderConverter.converter(order));
        }
        return ordersDTO;
    }

    @Override
    @Transactional
    public void updateOrderStatus(OrderDTO orderDTO) {
        Order order = OrderConverter.convertForUpdate(orderDTO);
        orderDao.saveOrUpdate(order);
        logger.info("Order updated");
    }


    @Override
    @Transactional
    public void saveOrder() {
        User user = userDao.findById(Principal.getPrincipal().getUserId());
        List<Basket> baskets = basketDao.findBasketByUserId(Principal.getPrincipal().getUserId());
        Order order = new Order();
        Set<OrderInfo> orderInfoSet = new HashSet<>();
        Double totalCost = 0.0;
        for (Basket basket : baskets) {
            totalCost = totalCost + basket.getQuantity() * basket.getBook().getPrice();
        }
        order.setStatus(Status.NEW);
        order.setTotalCost(totalCost);
        order.setUser(user);
        orderDao.save(order);
        logger.info("Order saved");

        for (Basket basket : baskets) {
            OrderInfo orderInfo = new OrderInfo();
            orderInfo.setBook(basket.getBook());
            orderInfo.setQuantity(basket.getQuantity());
            orderInfo.setPrice(basket.getBook().getPrice());
            orderInfo.setOrder(order);
            orderInfoDao.save(orderInfo);
            logger.info("OrderInfo saved");
            orderInfoSet.add(orderInfo);
        }
        order.setOrderInfoSet(orderInfoSet);
        orderDao.saveOrUpdate(order);
        for (Basket basket : baskets) {
            basketDao.delete(basket);
        }
    }

    @Override
    @Transactional
    public OrderDTO findOrderByOrderId(Integer id) {
        Order order = orderDao.findById(id);
        return OrderConverter.converter(order);
    }
}
