package com.gmail.olga.pilipets.dao;


import com.gmail.olga.pilipets.model.User;

public interface UserDao extends GenericDao<User, Integer>{

    /*User get(String email);
    void changeRole(User user, Role role);
    void changePassword(User user, String password);
    void blockUser(User user);
    void deleteUser(User user);*/
    User getUserByUsername(String username);

}
