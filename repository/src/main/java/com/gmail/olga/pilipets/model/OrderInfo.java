package com.gmail.olga.pilipets.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "T_ORDER_INFO")
public class OrderInfo implements Serializable {

    private static final long serialVersionUID = 5222338065413792661L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ORDER_INFO_ID")
    private Integer orderInfoId;
    @Column(name = "F_QUANTITY")
    private int quantity;
    @Column(name = "F_PRICE")
    private double price;
    @ManyToOne
    @JoinColumn(name = "F_BOOK_ID", nullable = false)
    private Book book;
    @ManyToOne
    @JoinColumn(name = "F_ORDER_ID", nullable = false)
    private Order order;


    public Integer getOrderInfoId() {
        return orderInfoId;
    }

    public void setOrderInfoId(Integer orderInfoId) {
        this.orderInfoId = orderInfoId;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderInfo orderInfo = (OrderInfo) o;
        return Objects.equals(orderInfoId, orderInfo.orderInfoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderInfoId);
    }
}
