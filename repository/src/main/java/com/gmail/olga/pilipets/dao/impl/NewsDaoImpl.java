package com.gmail.olga.pilipets.dao.impl;

import com.gmail.olga.pilipets.dao.NewsDao;
import com.gmail.olga.pilipets.model.News;
import org.springframework.stereotype.Repository;

@Repository("newsDao")
public class NewsDaoImpl extends GenericDaoImpl <News,Integer> implements NewsDao {


}
