package com.gmail.olga.pilipets.dao.impl;

import com.gmail.olga.pilipets.dao.UserDao;
import com.gmail.olga.pilipets.model.User;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

@Repository("userDao")
public class UserDaoImpl extends GenericDaoImpl<User, Integer> implements UserDao {
    @Autowired
    private Environment environment;

    @Override
    public User getUserByUsername(String username) {
        String hql = environment.getRequiredProperty("user.by.username");
        Query query = getSession().createQuery(hql);
        query.setParameter("username", username);
        Object uniqueResult = query.uniqueResult();
        return uniqueResult != null ? (User) uniqueResult : null;
    }

}
