package com.gmail.olga.pilipets.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "T_BOOK")
public class Book implements Serializable {

    private static final long serialVersionUID = 7955725452613485715L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_BOOK_ID", nullable = false)
    private Integer bookId;
    @Column(name = "F_TITLE")
    private String title;
    @Column(name = "F_AUTHOR")
    private String author;
    @Column(name = "F_DESCRIPTION")
    private String description;
    @Column(name = "F_PRICE")
    private Double price;
    @OneToMany(mappedBy="book", cascade = {CascadeType.ALL})
    private List<Basket> baskets;
    @OneToMany(mappedBy="book", cascade = {CascadeType.ALL})
    private List<OrderInfo> orderInfos;


    public List<OrderInfo> getOrderInfos() {
        return orderInfos;
    }

    public void setOrderInfos(List<OrderInfo> orderInfos) {
        this.orderInfos = orderInfos;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<Basket> getBaskets() {
        return baskets;
    }

    public void setBaskets(List<Basket> baskets) {
        this.baskets = baskets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(bookId, book.bookId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookId);
    }
}
