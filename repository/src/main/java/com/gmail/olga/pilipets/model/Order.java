package com.gmail.olga.pilipets.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "T_ORDER")
public class Order implements Serializable {

    private static final long serialVersionUID = 6525216026512836636L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ORDER_ID", nullable = false)
    private Integer orderId;
    @Column(name = "F_STATUS")
    @Enumerated(EnumType.STRING)
    private Status status;
    @Column(name = "F_TOTAL_COST")
    private Double totalCost;
    @ManyToOne
    @JoinColumn(name = "F_USER_ID", nullable = false)
    private User user;
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private Set<OrderInfo> orderInfoSet=new HashSet<>();

    public Set<OrderInfo> getOrderInfoSet() {
        return orderInfoSet;
    }

    public void setOrderInfoSet(Set<OrderInfo> orderInfoSet) {
        this.orderInfoSet = orderInfoSet;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(orderId, order.orderId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId);
    }


}
