package com.gmail.olga.pilipets.dao.impl;

import com.gmail.olga.pilipets.dao.PhotoDao;
import com.gmail.olga.pilipets.model.Photo;
import org.springframework.stereotype.Repository;

@Repository("photoDao")
public class PhotoDaoImpl extends GenericDaoImpl<Photo, Integer>implements PhotoDao {
}
