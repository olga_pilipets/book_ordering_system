package com.gmail.olga.pilipets.dao.impl;

import com.gmail.olga.pilipets.dao.BookDao;
import com.gmail.olga.pilipets.model.Book;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("bookDao")

public class BookDaoImpl extends GenericDaoImpl<Book, Integer> implements BookDao {
    private static final Logger logger = Logger.getLogger(BookDaoImpl.class);

    @Autowired
    private Environment environment;

    @Override
    public void saveOrUpdateBook(Book book) {
        String hql = environment.getRequiredProperty("if.book.in.order");
        Query query = getSession().createQuery(hql);
        query.setParameter("id", book.getBookId());
        List list = query.list();
        if (list.isEmpty()) {
            getSession().update(book);

        } else {
            logger.info("Book is in order. Can not be updated");
        }
    }

}
