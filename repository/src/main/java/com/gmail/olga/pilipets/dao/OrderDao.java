package com.gmail.olga.pilipets.dao;

import com.gmail.olga.pilipets.model.Order;

import java.util.List;

public interface OrderDao extends GenericDao<Order, Integer> {
    List<Order> findOrdersByUserId(Integer userId);


}
