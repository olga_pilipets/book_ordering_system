package com.gmail.olga.pilipets.dao;

import com.gmail.olga.pilipets.model.Feedback;

public interface FeedbackDao extends GenericDao<Feedback, Integer> {
}
