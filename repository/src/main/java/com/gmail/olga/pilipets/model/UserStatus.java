package com.gmail.olga.pilipets.model;

public enum UserStatus {
    BLOCKED,
    ACTIVE
}
