package com.gmail.olga.pilipets.model;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "T_BASKET")
public class Basket implements Serializable {

    private static final long serialVersionUID = 4251316103366450911L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_BASKET_ID", nullable = false)
    private Integer basketId;
    @Column(name = "F_QUANTITY")
    private Integer quantity;
    @ManyToOne
    @JoinColumn(name = "F_USER_ID", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "F_BOOK_ID", nullable = false)
    private Book book;

    public Integer getBasketId() {
        return basketId;
    }

    public void setBasketId(Integer basketId) {
        this.basketId = basketId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
