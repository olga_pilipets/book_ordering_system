package com.gmail.olga.pilipets.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table
public class Photo implements Serializable{
    private static final long serialVersionUID = 1554558929573262164L;
    @Id
    @GeneratedValue
    @Column(name = "F_NEWS_ID", nullable = false)
    private Integer newsId;
    @Column(name = "F_FILE_NAME")
    private String photoName;
    @Column(name = "F_LOCATION")
    private String location;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "photo")
    private News news;

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public String getPhotoName() {
        return photoName;
    }

    public void setPhotoName(String fileName) {
        this.photoName = fileName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Photo photo = (Photo) o;
        return Objects.equals(newsId, photo.newsId) &&
                Objects.equals(photoName, photo.photoName) &&
                Objects.equals(location, photo.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(newsId, photoName, location);
    }
}
