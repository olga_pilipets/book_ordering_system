package com.gmail.olga.pilipets.dao.impl;

import com.gmail.olga.pilipets.dao.OrderInfoDao;
import com.gmail.olga.pilipets.model.OrderInfo;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository("orderInfoDao")
public class OrderInfoDaoImpl extends GenericDaoImpl<OrderInfo, Integer> implements OrderInfoDao {

    private static final Logger logger = Logger.getLogger(OrderInfoDaoImpl.class);

    @Autowired
    private Environment environment;
    @Override
    public List<OrderInfo> findAllByOrderId(Integer id) {
        String hql = environment.getRequiredProperty("info.by.order.id");
        Query query = getSession().createQuery(hql);
        query.setParameter("id", id);
        List<OrderInfo> list = query.list();
        if (list.isEmpty()) {
            logger.info("Can not find info to order");
        }
        return list;
    }

}
