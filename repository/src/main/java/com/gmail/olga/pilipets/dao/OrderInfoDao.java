package com.gmail.olga.pilipets.dao;

import com.gmail.olga.pilipets.model.OrderInfo;

import java.util.List;

public interface OrderInfoDao  extends GenericDao<OrderInfo, Integer>{
    List<OrderInfo> findAllByOrderId(Integer id);

}
