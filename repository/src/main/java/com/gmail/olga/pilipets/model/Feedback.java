package com.gmail.olga.pilipets.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "T_FEEDBACK")
public class Feedback implements Serializable {
    private static final long serialVersionUID = 5102286357514813588L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ID", nullable = false)
    private Integer id;
    @Column(name = "F_EMAIL")
    private String email;
    @Column(name = "F_CONTENT")
    private String content;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
