package com.gmail.olga.pilipets.dao.impl;

import com.gmail.olga.pilipets.dao.FeedbackDao;
import com.gmail.olga.pilipets.model.Feedback;
import org.springframework.stereotype.Repository;

@Repository("feedbackDao")
public class FeedbackDaoImpl extends GenericDaoImpl <Feedback,Integer> implements FeedbackDao {
}
