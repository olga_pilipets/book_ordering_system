package com.gmail.olga.pilipets.dao.impl;

import com.gmail.olga.pilipets.dao.BasketDao;
import com.gmail.olga.pilipets.model.Basket;
import com.gmail.olga.pilipets.model.Book;
import com.gmail.olga.pilipets.model.User;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("basketDao")

public class BasketDaoImpl extends GenericDaoImpl<Basket, Integer> implements BasketDao {

    private static final Logger logger = Logger.getLogger(BasketDaoImpl.class);

    @Autowired
    private Environment environment;

    @Override
    public List<Basket> findBasketByUserId(Integer id) {
        String hql = environment.getRequiredProperty("basket.by.user.id");
        Query query = getSession().createQuery(hql);
        query.setParameter("id", id);
        List<Basket> list = query.list();
        if (list.isEmpty()){
            logger.info("User basket is empty");
        }
        return list;
    }

    @Override
    public void addBooktoBasket(Book book, User user) {
        Basket basket = new Basket();
        basket.setBook(book);
        basket.setUser(user);
        basket.setQuantity(1);
        getSession().save(basket);
    }


}
