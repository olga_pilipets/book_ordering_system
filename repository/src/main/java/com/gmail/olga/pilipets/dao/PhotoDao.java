package com.gmail.olga.pilipets.dao;


import com.gmail.olga.pilipets.model.Photo;

public interface PhotoDao extends GenericDao<Photo, Integer>{
}
