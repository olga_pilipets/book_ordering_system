package com.gmail.olga.pilipets.dao.impl;

import com.gmail.olga.pilipets.dao.OrderDao;
import com.gmail.olga.pilipets.model.Order;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("orderDao")

public class OrderDaoImpl extends GenericDaoImpl<Order, Integer> implements OrderDao {

    private static final Logger logger = Logger.getLogger(OrderDaoImpl.class);

    @Autowired
    private Environment environment;

    @Override
    public List<Order> findOrdersByUserId(Integer userId) {
        String hql = environment.getRequiredProperty("orders.by.user.id");
        Query query = getSession().createQuery(hql);
        query.setParameter("id", userId);
        List<Order> list = query.list();
        if (list.isEmpty()) {
            logger.info("User has no orders");
        }
        return list;

    }


}
