package com.gmail.olga.pilipets.dao;

import com.gmail.olga.pilipets.model.Book;


public interface BookDao extends GenericDao<Book, Integer>{
    void saveOrUpdateBook(Book book);
    void delete(Book book);
}
