package com.gmail.olga.pilipets.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "T_USER")

public class User implements Serializable {

    private static final long serialVersionUID = 6670095918741062500L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_USER_ID", nullable = false)
    private Integer id;
    @Column(name = "F_NAME")
    private String name;
    @Column(name = "F_SURNAME")
    private String surname;
    @Column(name = "F_FATHERNAME")
    private String fathername;
    @Column(name = "F_USERNAME", nullable = false, unique = true)
    private String username;
    @Column(name = "F_PHONE")
    private String phone;
    @Column(name = "F_ADDRESS")
    private String address;
    @Column(name = "F_PASSWORD")
    private String password;
    @Column(name = "F_ROLE")
    @Enumerated(EnumType.STRING)
    private Role role;
    @Column(name = "F_STATUS")
    @Enumerated(EnumType.STRING)
    private UserStatus userStatus;
    @Column(name = "F_EXTRA_INFO")
    private String extraInfo;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Order> orderSet = new HashSet<>();

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Basket> baskets;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFathername() {
        return fathername;
    }

    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<Basket> getBaskets() {
        return baskets;
    }

    public void setBaskets(List<Basket> baskets) {
        this.baskets = baskets;
    }

    public Set<Order> getOrderSet() {
        return orderSet;
    }

    public void setOrderSet(Set<Order> orderSet) {
        this.orderSet = orderSet;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(username, user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username);
    }
}