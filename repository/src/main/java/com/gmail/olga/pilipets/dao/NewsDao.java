package com.gmail.olga.pilipets.dao;


import com.gmail.olga.pilipets.model.News;

public interface NewsDao extends GenericDao<News, Integer>{

}
