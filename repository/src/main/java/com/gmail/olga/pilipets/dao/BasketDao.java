package com.gmail.olga.pilipets.dao;


import com.gmail.olga.pilipets.model.Basket;
import com.gmail.olga.pilipets.model.Book;
import com.gmail.olga.pilipets.model.User;

import java.util.List;

public interface BasketDao extends GenericDao<Basket, Integer>{
    List<Basket> findBasketByUserId(Integer id);
    void addBooktoBasket(Book book, User user);

}
