package com.gmail.olga.pilipets.model;


public enum Status {
    NEW,
    REVIEWING,
    IN_PROCESS,
    DELIVERED
}
