package com.gmail.olga.pilipets.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "T_NEWS")
public class News implements Serializable {

    private static final long serialVersionUID = -233752404645598096L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_NEWS_ID", nullable = false)
    private Integer newsId;
    @Column(name = "F_DATE")
    @Type(type = "date")
    private Date date;
    @Column(name = "F_NEWS_TITLE")
    private String newsTitle;
    @Column(name = "F_NEWS_TEXT")
    private String newsText;
    @OneToOne(cascade = CascadeType.ALL)
    private Photo photo;

    public Integer getNewsId() {
        return newsId;
    }

    public void setNewsId(Integer newsId) {
        this.newsId = newsId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsText() {
        return newsText;
    }

    public void setNewsText(String newsText) {
        this.newsText = newsText;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        News news = (News) o;
        return Objects.equals(newsId, news.newsId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(newsId);
    }
}
