package com.gmail.olga.pilipets.dao;


import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

public interface GenericDao <T extends Serializable, ID extends Serializable>{
   T findById (final ID id);
   ID save (T entity);
   void saveOrUpdate(T entity);
   void delete (T entity);
   void deleteAll ();
   List<T> findAll();
   void deleteById(ID id);
   void merge(T entity);

}
